var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var gcmq = require('gulp-group-css-media-queries');
var sass = require('gulp-sass');


//path
var paths = {
  scripts: 'src/js/**/*.js',
  images: 'src/image/**',
  sass: 'src/sass/**/*.scss'
};

//ease tasks
gulp.task('sass',  function () {
 return gulp.src(paths.sass)
  .pipe(sourcemaps.init())

  .pipe(sass().on('error', sass.logError))
  .pipe(gcmq())
  .pipe(autoprefixer())
  .pipe(concat('style.css'))

  .pipe(sourcemaps.write('./maps'))

  .pipe(gulp.dest('public/styles'));
});

gulp.task('scripts',  function() { 
  return gulp.src(paths.scripts)

    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('all.min.js'))
    .pipe(sourcemaps.write())
    
    .pipe(gulp.dest('public/js'));
});

// Copy all static images
gulp.task('images',  function() {
  return gulp.src(paths.images)
    // Pass in options to the task
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest('public/image'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.sass, ['sass']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'sass', 'scripts', 'images']);